#pragma once

#include "ofMain.h"
#include "ofxGlyphFlex.h"
#include "OpenOfxTrueTypeFontUC.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    float GeronFunc(float a,float b,float c)
    {
        float p = (a+b+c)/2;
        return sqrt(p*(p-a)*(p-b)*(p-c));
    }
    
    float quadArea(ofPoint p1, ofPoint p2, ofPoint p3, ofPoint p4) {
//        S=GeronFunc(ab,bd,da)+GeronFunc(bc,cd,bd);
        float l1 = (p2 - p1).length();
        float l2 = (p3 - p2).length();
        float l3 = (p4 - p3).length();
        float l4 = (p1 - p4).length();
        return GeronFunc(l1, l2, (p4 - p2).length()) +
               GeronFunc(l1, l2, (p3 - p1).length());
    }
    
    SvgMovie movie;
    
    vector<TextFlexer> flexers;
    
    vector<float> desiredHeights;
    
    OpenOfxTrueTypeFontUC font;
    
    int selectedLetter;
    
    ofRectangle tMatrix;
    
    ofxXmlSettings settings;
    
    string myString;
    
    ofxPanel gui;
    ofxFloatSlider textSizeSlider, phraseBegin, phraseEnd, lineHeight;
    ofxFloatSlider sizeAnimationAmount;
    ofxFloatSlider kern;
    ofxToggle showOutline;
    
};
