#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    int FRAME_COUNT = 3; // сколько кадров будет в ролике? задайте количество здесь.
    for (int i = 1; i < FRAME_COUNT + 1; i++) {
        ofxSVG frame;
        frame.load("b" + ofToString(i) + ".svg");
        movie.pushBack(frame);
        ofLog() << "loaded " << "b" << ofToString(i) << ".svg";
    }

    font.loadFont("frabk.ttf", 50, false, true, 0);
    settings.load("settings.xml");
    myString = settings.getValue("phrase", "no file");
    
    for (int i = 0; i < movie.frames[0].getNumPath(); i++) {
        TextFlexer addon;
        addon.font = new OpenOfxTrueTypeFontUC();
        addon.font->loadFont("frabk.ttf", 50, false, true, 0);
        addon.setMovie(&movie);
        addon.pathIndexInMovie = i;
        addon.createLetters(myString);
        flexers.push_back(addon);
    }
    
    gui.setup();
    gui.add(textSizeSlider.setup("letter size in pixels", 10, 1, 20));
    gui.add(phraseBegin.setup("phrase begin", 0, 0, 1));
    gui.add(phraseEnd.setup("phrase end", 0.66, 0, 1));
    gui.add(lineHeight.setup("line height", 0.65, 0, 2));
    gui.add(sizeAnimationAmount.setup("size animation amount", 0.05, 0, 1));
    gui.add(kern.setup("kern", 1.2, 0.1, 2.5));
    gui.add(showOutline.setup("show outline", false));
}

//--------------------------------------------------------------
void ofApp::update(){
    
    float speed = 0.5;
    float fI = (ofGetElapsedTimef()/(10 / speed) - (int)(ofGetElapsedTimef()/(10 / speed))) * movie.frames.size();
    movie.setFrameIndex(fI); // текущий кадр в ролике из серии svg файлов.
                             // скорость задается чуть выше, в переменной speed

    
    while (desiredHeights.size() > 0) desiredHeights.pop_back();
    for (int i = 0; i < 100; i++) {
        float addon =
        (((sin((float)i/ 3 + ofGetElapsedTimef() * 5) * 0.3 + 0.3 + 0.3) * sizeAnimationAmount) + (1 - sizeAnimationAmount)) * lineHeight;
        desiredHeights.push_back(addon);
    }

    for (int i = 1; i < flexers.size(); i++) {
        flexers[i].update();
        flexers[i].setLettersPositionsAnimated(textSizeSlider, ofGetElapsedTimef(), 0.01, desiredHeights, phraseBegin, phraseEnd);
        // все блоки текста располагаются вдоль кривой. пояснения к функции в .h файле
    }
    flexers[0].update();
    flexers[0].setLettersPositionsAsRunningLine(textSizeSlider, phraseBegin/*ofGetElapsedTimef() / 10*/, kern);
    
    // Чтобы строка бежала, лучше следующее:
    /*    flexers[0].setLettersPositionsAsRunningLine(textSizeSlider, ofGetElapsedTimef() / 10, kern);*/
    // первый блок текста является бегущей строкой. второй параметр функции - скорость бега
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(255);


    ofPushMatrix();
        ofTranslate(100, 100);
        ofTranslate(100, 600);
        ofRotate(-90, 0,0,1);
        ofSetColor(80, 80);
    ofPopMatrix();
    
    ofPushMatrix();
        ofTranslate(100, 100);
        ofTranslate(100, 600);
        ofRotate(-90, 0,0,1);
        ofTranslate(tMatrix.position);
        ofScale(1.0 + tMatrix.width, 1.0 + tMatrix.width);
        ofSetColor(0);
        for (int i = 0; i < flexers.size(); i++) {
            for (int j = 0; j < flexers[i].letters.size(); j++) {
                        ofSetColor(0);
                        flexers[i].letters[j]->draw();
                
                        ofNoFill();
                        ofSetColor(0, 0, 0, 200);
                if (showOutline) {
                    ofBeginShape();
                        ofVertex(flexers[i].letters[j]->targetPosition[0]);
                        ofVertex(flexers[i].letters[j]->targetPosition[1]);
                        ofVertex(flexers[i].letters[j]->targetPosition[2]);
                        ofVertex(flexers[i].letters[j]->targetPosition[3]);
                        ofVertex(flexers[i].letters[j]->targetPosition[0]);
                    ofEndShape();
                }
                
                ofFill();
                
            }
        }
    ofPopMatrix();
    
    gui.draw();
    float tssv = textSizeSlider;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == OF_KEY_RIGHT) {
        tMatrix.position.x += 5;
    }
    if (key == OF_KEY_LEFT) {
        tMatrix.position.x -= 5;
    }
    if (key == OF_KEY_UP) {
        tMatrix.position.y -= 5;
    }
    if (key == OF_KEY_DOWN) {
        tMatrix.position.y += 5;
    }
    
    if (key == '+') tMatrix.width += 0.1;
    if (key == '-') tMatrix.width -= 0.1;
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
