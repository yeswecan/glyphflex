//
//  TextFlexer.cpp
//
//  Created by zebra on 05/03/15.
//
//

#ifndef openFrameworksLib_TextFlexer_cpp
#define openFrameworksLib_TextFlexer_cpp


#include "TextFlexer.h"


bool TextFlexer::areLettersAnimating() {
    for (int i = 0; i < letters.size(); i++) if (letters[i]->isAnimating) return true;
    return false;
}
ofPoint TextFlexer::getTopPointAtPercent(float percent) {
    return line_top.getPointAtPercent(percent);
}

ofPoint TextFlexer::getBottomPointAtPercent(float percent) {
    return line_bottom.getPointAtPercent(percent);
}

float TextFlexer::getTopPerimeter() {
    return line_top.getPerimeter();
}

float TextFlexer::getBottomPerimeter() {
    return line_bottom.getPerimeter();
}

void TextFlexer::setWidthMultiplier(float arg) {
    widthMultiplier = arg;
}

float TextFlexer::getLength() {
    float sum = 0;
    for (int i = 0; i < letters.size(); i++) {
        sum += letters[i]->kerning;
    }
    
    return sum * widthMultiplier;
}

void TextFlexer::setLettersPositionsAnimated(float desiredWidth, float animationStartedTime, float animationTimeframe, vector<float> desiredHeights, float start, float end, bool useHeightMap, float desiredHeight) {
    float kernings[200]; float sum = 0;
    for (int i = 0; i < letters.size(); i++) {
        kernings[i] = letters[i]->kerning;
        sum += letters[i]->kerning;
    }
    for (int i = 0; i < letters.size(); i++) kernings[i] = kernings[i] / sum;
    float kerningPosition = 0;
    float selectedLineLength = end - start;
    
    float topLength = getTopPerimeter();
    float bottomLength = getBottomPerimeter();
    float length = (topLength < bottomLength ? topLength * (end - start) : bottomLength * (end - start));
    for (int i = 0; i < letters.size(); i++) {
        
        float letterWidth = desiredWidth;
        if (desiredWidth == 0)
            letterWidth = 40;
        if ((length / (float)letters.size()) < letterWidth) letterWidth = length / (float)letters.size();
        float letterLength = letterWidth / length;
        
        //float p1 = (1. / ((float)arg.length() - 1)) * (float)i;
        float q = 1 - ((float)i / (float)letters.size());
        float p1 = start + (kerningPosition * selectedLineLength);
        float p2 = (p1 + letterLength * selectedLineLength);
        if ((p1 > 0.999) && (p2 > 0.999)) {
            //                ofLog() << "letter " << letters[i]->letter << " exceeds " << ofRandom(255);
            letters[i]->visible = false;
        } else
            if ((p1 < 0.001) && (p2 < 0.001)) {
                //                ofLog() << "letter " << letters[i]->letter << " preceeds " << ofRandom(255);
                letters[i]->visible = false;
            } else {
                letters[i]->visible = true;
            }
        p2 = ofClamp(p2, 0, 0.99);
        
        if (useTargetStartPosition) {
            letters[i]->previousPosition[0] = letters[i]->targetPosition[0];
            letters[i]->previousPosition[1] = letters[i]->targetPosition[1];
            letters[i]->previousPosition[2] = letters[i]->targetPosition[2];
            letters[i]->previousPosition[3] = letters[i]->targetPosition[3];
        } else {
            letters[i]->previousPosition[0] = letters[i]->currentPosition[0];
            letters[i]->previousPosition[1] = letters[i]->currentPosition[1];
            letters[i]->previousPosition[2] = letters[i]->currentPosition[2];
            letters[i]->previousPosition[3] = letters[i]->currentPosition[3];
        }
        letters[i]->targetPosition[0] = getTopPointAtPercent(p1);
        letters[i]->targetPosition[1] = getTopPointAtPercent(p2);
        letters[i]->targetPosition[2] = getBottomPointAtPercent(p2);
        letters[i]->targetPosition[3] = getBottomPointAtPercent(p1);
        letters[i]->animationTimeframe = animationTimeframe;
        letters[i]->animationStartedTime = animationStartedTime + i * lettersTimeOffset;
        
        kerningPosition += kernings[i];
    };
}

void TextFlexer::setLettersPositionsAnimated(float desiredWidth, float animationStartedTime, float animationTimeframe, float start, float end, float desiredHeight) {
    vector<float> nullvector;
    setLettersPositionsAnimated(desiredWidth, animationStartedTime, animationTimeframe, nullvector, start, end, false, desiredHeight);
}

// TODO: letters time offset not implemented
void TextFlexer::setLettersPathsAnimated(ofPolyline top, ofPolyline bottom, float animationStartedTime, float animationTimeframe, float pathStart, float pathEnd, float textWidth, float lettersTimeOffset_) {
    innerHost.setupWithTwoLines(top, bottom);
    TextCarrier *tc = innerHost.assignNewCarrier(1,0);
    animateToCarrier(tc, animationTimeframe, animationStartedTime);
}

void TextFlexer::animateToCarrier(TextCarrier *tc, float animationTime, float animationStartTime, float perLetterOffset, bool startFromCurrentPoints) {
    float kernings[200]; float sum = 0;
    for (int i = 0; i < letters.size(); i++) {
        kernings[i] = letters[i]->kerning;
        sum += letters[i]->kerning;
    }
    for (int i = 0; i < letters.size(); i++) kernings[i] = kernings[i] / sum;
    float kerningPosition = 0;

    for (int i = 0; i < letters.size(); i++) {
        
        
        float q = 1 - ((float)i / (float)letters.size());
        float p1 = kerningPosition;
        float p2 = (p1 + kernings[i]);
        letters[i]->animateTo(tc, animationStartTime + perLetterOffset * i, animationTime, p1, p2-p1, startFromCurrentPoints);
        
        kerningPosition += kernings[i];
    }
    
    targetCarrier = tc;
}

void TextFlexer::createLetters(string arg) {
    for (int i = 0; i < letters.size(); i++) {
        delete letters[i];
    }
    while (letters.size() > 0) letters.pop_back();
    
    basic_string_text = arg;
    
    basic_string<unsigned int> utf32_src = util::OpenOfxTrueTypeFontUC::convUTF8ToUTF32(arg);
    int len = (int)utf32_src.length();
    
    text = utf32_src;
    ofLog() << "creating letters for " << text.c_str();
    
    int i = 0;
    int kIndex = 0;
	float kernings[200]; // it was float kernings[arg.length()];, but it doesn't compile on windows
	float sum = 0;
    float kerningPosition = 0;
    
    //        float selectedLineLength = end - start;
    
    
    i = 0;
    kIndex = 0;
    while (i < arg.length()) {
        string symbol;// = arg;
        
        int kerning;
        
        wchar_t c = (unsigned char)arg[i];
        if (((unsigned int)(c) > 0x80)) { // cyrillic
            symbol = arg.substr(i, 2);
            kerning = font->getTTFKerning(utf32_src[kIndex]) * 0.7;
            ofLog() << i << ": " << arg.substr(i, 2) << " : cyrillic kerning is " << kerning;
            i += 2;
        } else {                          // latin
            symbol = arg.substr(i, 1);
            kerning = font->getTTFKerning(utf32_src[kIndex]) * 0.7;
            ofLog() << i << ": " << arg[i] << " : latin kerning is " << kerning;
            i++;
        }
        
        FlexLetter *addon = new FlexLetter(symbol);
        addon->warper = warper_;
        addon->kerning = kerning;
        addon->font = font;
//        addon->useCache = false;
        
        letters.push_back(addon);
        
        kIndex++;
    };
}

void TextFlexer::drawLetters(bool useInnerColor) {
    for (int i = 0; i < letters.size(); i++) {
        if (useInnerColor) {
            ofSetColor(letters[i]->color);
        }
        letters[i]->draw();
    }
}

void TextFlexer::update() {
    for (int i = 0; i < letters.size(); i++) letters[i]->update();
    innerHost.update();
    

}

int TextFlexer::getShortestIndex(vector<line> lines_) {
    int shortestIndex = -1; float shortestLength = 999999;
    for (int i = 0; i < lines_.size(); i++) {
        ofLog() << "lines[" << i << "] = " << lines_[i].start << " - " << lines_[i].end << " , len = " << (lines_[i].end - lines_[i].start).length();
        if ((lines_[i].end - lines_[i].start).length() < shortestLength) {
            shortestIndex = i; shortestLength = (lines_[i].end - lines_[i].start).length();
        }
    }
    ofLog() << "found shortest! " <<  shortestIndex << " , from " << lines_[shortestIndex].start << " to " << lines_[shortestIndex].end;
    return shortestIndex;
}

void TextFlexer::translatePaths(ofPoint factor) { // it just moves the path by the argument vector
    for (int i = 0; i < line_top.size(); i++) {
        line_top[i] += factor;
    }
    for (int i = 0; i < line_bottom.size(); i++) {
        line_bottom[i] += factor;
    }
}

void TextFlexer::drawOutline() {
    ofSetColor(0, 255, 0);
    line_top.draw();
    ofSetColor(0, 0, 255);
    line_bottom.draw();
    
    for (int i = 0; i < letters.size(); i++) {
        ofSetColor(0);
        ofLine(letters[i]->targetPosition[0], letters[i]->targetPosition[2]);
        ofLine(letters[i]->targetPosition[1], letters[i]->targetPosition[3]);
    }
}

string TextFlexer::getText() {
    return basic_string_text;
}


#endif
