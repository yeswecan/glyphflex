//
//  SvgMovie.h
//  bezierParser
//
//  Created by zebra on 30/04/15.
//
//

#ifndef bezierParser_SvgMovie_h
#define bezierParser_SvgMovie_h

#include "ofxSvg.h"
#include "TextCarrierSystem.h"

class SvgMovieSystem : public TextCarrierSystem {
    
public:
    SvgMovieSystem() {
        speed = 0.5;
        
        multiplier = ofPoint(1);
    }
    
    // SVGMOVIE:
    
    void pushFrame(ofxSVG frame) {
        if (frames.size() == 0) {
            recreateLines(frame);
        }
        
        frames.push_back(frame);
        FRAME_COUNT = frames.size();
    }
    
    void recreateLines(ofxSVG frame) {
        for (int i = 0; i < frame.getNumPath(); i++) {
            addLine(frame.getPathAt(i).getOutline()[0]);
        }
    }
    
    void pushFrame(string filename) {
        ofxSVG svg;
        svg.load(filename);
        pushFrame(svg);
    }
    
    void setFrameIndex(float fI_) {
        fI = fI_;
    }
    

    float floatPart(float arg) {
        return arg - (int)arg;
    }
    
    float time, fI;
    int FRAME_COUNT;
    
    vector<ofxSVG> frames;
    
    void setLinesHeight(float height) {
        for (int i = 0; i < lines.size(); i++) {
            lines[i].height = height;
        }
    }

    void setLinesColor(ofColor color) {
        for (int i = 0; i < lines.size(); i++) {
            lines[i].color = color;
        }
    }
    
    float getFrameTransition() {
        return fI - (int)fI;
    }
    
    int getFirstFrameIndex() {
        return (int)fI;
    }

    int getSecondFrameIndex() {
        int secondFrameIndex = (int)fI + 1;
        if (((int)fI + 1) > frames.size() - 1) secondFrameIndex = 0;
        return secondFrameIndex;
    }
    
    void update() override {
        TextCarrierSystem::update();
        
        fI = (ofGetElapsedTimef()/(10 / speed) - (int)(ofGetElapsedTimef()/(10 / speed))) * frames.size();
        
        int firstFrame = getFirstFrameIndex();
        int secondFrame = getSecondFrameIndex();
        float transition = getFrameTransition();
        
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines[i].centralPolyline.size(); j++) {
                lines[i].centralPolyline[j] = (frames[firstFrame].getPathAt(i).getOutline()[0][j] * (1 - transition) + frames[secondFrame].getPathAt(i).getOutline()[0][j] * ( transition) ) * sizeModifier + offsetModifier;
            }
        }
    }
    
    float speed;
    
    ofPoint offset;
    ofPoint multiplier;

};
#endif
