#ifndef MyGLWarper_h
#define MyGLWarper_h


#include "ofxGLWarper.h"
#include <limits>

class MyGLWarper : public ofxGLWarper {
	int whichCornerMy;
    ofPoint point;
	GLfloat myMatrix[16];

public:
	void mouseDragged(ofMouseEventArgs &args);
	void mousePressed(ofMouseEventArgs &args);
	void mouseReleased(ofMouseEventArgs &args);
	void mouseMoved(ofMouseEventArgs &args);

    
	void activate();
	void deactivate();
    
    ofMatrix4x4 homography;
    ofPoint originalCorners[4], distortedCorners[4];
};

#endif