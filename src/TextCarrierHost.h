//
//  TextCarrierHost.h
//  TextMagicAPI
//
//  Created by zebra on 01/10/15.
//
//

#ifndef TextMagicAPI_TextCarrierHost_h
#define TextMagicAPI_TextCarrierHost_h

#include "TextCarrier.h"
#include "TextCarrierDataProvider.h"

class TextCarrierHost: public TextCarrierDataProvider {
public:
    TextCarrierHost() {
        centralPolyline.addVertex(ofPoint(0, 0));
        centralPolyline.addVertex(ofPoint(200, 200));
        
        useCentral = true;

        height = 10;
        
        overflowFlag = false;
    }
    
    ~TextCarrierHost() {
        for (int i = 0; i < carriers.size(); i++) {
            delete carriers[i];
        }
    }
    
    
    // Data provider:
    float getLengthInPixels() {
        if (useCentral) return centralPolyline.getLengthAtIndex(centralPolyline.size() - 1);
            else return topPolyline.getLengthAtIndex(topPolyline.size() - 1);
    }

    ofPoint getPointAtLeft(float position) {
        if (!useCentral) return topPolyline.getPointAtPercent(position);
        if ((position) >= (1 - 0.001)) {
                return Utils::getVectorRotated(
                                               centralPolyline.getPointAtPercent(1 - 0.001),
                                               centralPolyline.getPointAtPercent(1),
                                               90, getHeightAtPercent(position));
    
            }
            return Utils::getVectorRotated(
                                    centralPolyline.getPointAtPercent(position),
                                    centralPolyline.getPointAtPercent(position + 0.01),
                                    -90, getHeightAtPercent(position));
    }

    ofPoint getPointAtRight(float position) {
        if (!useCentral) {
            return bottomPolyline.getPointAtPercent(position);
        }
        if ((position) >= (1 - 0.001)) {
            return Utils::getVectorRotated(
                                           centralPolyline.getPointAtPercent(1 - 0.001),
                                           centralPolyline.getPointAtPercent(1),
                                           -90, getHeightAtPercent(position));
            
        }
        return Utils::getVectorRotated(
                                       centralPolyline.getPointAtPercent(position),
                                       centralPolyline.getPointAtPercent(position + 0.01),
                                       90, getHeightAtPercent(position));
    }
    
    virtual float getHeightAtPercent(float position) {
        return height;
    };
    
    float internalOffset = 0;
    
    // 4 points at once produce better result, cause it calculates positions in relation to the
    // center of the requested region. If you call it this way then the letters will never curl or skew.
    virtual void get4Points(ofPoint *points, float start, float length) {
        
        start += internalOffset;
        
        while ((start) > 1) {
            start -= 1;
        }
        if (!useCentral) {
            points[0] = getPointAtLeft(start);
            points[1] = getPointAtLeft(start + length);
            points[2] = getPointAtRight(start + length);
            points[3] = getPointAtRight(start);
 
        } else {
            ofPoint p1 = centralPolyline.getPointAtPercent(start); 
            ofPoint p2 = centralPolyline.getPointAtPercent(start + length);
            ofPoint central = (p2 + p1) / 2;
            float letterWidth = (p2 - p1).length();
            
            points[0] = Utils::getVectorRotated(central, central + (p2 - p1) / 100, -90, getHeightAtPercent(start + length/2)) - (p2 - p1)/2;
            points[1] = Utils::getVectorRotated(central, central + (p2 - p1) / 100, -90, getHeightAtPercent(start + length/2)) + (p2 - p1)/2;
            points[2] = Utils::getVectorRotated(central, central + (p2 - p1) / 100, 90, getHeightAtPercent(start + length/2)) + (p2 - p1)/2;
            points[3] = Utils::getVectorRotated(central, central + (p2 - p1) / 100, 90, getHeightAtPercent(start + length/2)) - (p2 - p1)/2;
            
            // TODO:
            // 1. Smaller points if start + length exceeds 1
            // 2. Start from the beginning if it's even more
        }
        // the following makes the letter smaller as the go out of reach:
        ofPoint center = (points[0] + points[1] + points[2] + points[3]) / 4;
        if ((1 - start) < length) {
            for (int i = 0; i < 4; i++) {
                points[i] += (center - points[i]) * (1 - (1 - start) / length);
            }
        }
        if ((start) < 0) {
            for (int i = 0; i < 4; i++) {
                points[i] += (center - points[i]) * ((0 - start) / length);
            }
        }
    };


    
    float height;
    
    // Other stuff
    // Setting up curves
    void setupWithPolyline(ofPolyline line) {
        centralPolyline.clear();
        centralPolyline = line;
    }
    
    void setupWithTwoLines(ofPolyline top, ofPolyline bottom) {
        useCentral = false;
        topPolyline = top;
        bottomPolyline = bottom;
    }
    
    float getMinimalPossibleDistanceToPoint(ofPoint target, int tries) {
        float result = 999999;
        for (int i = 0; i < tries; i++) {
            float position = ofRandomuf();
            ofPoint point;
            if (useCentral) {
                point = centralPolyline.getPointAtPercent(position);
                float distance = (point - target).length();
                if (distance < result) result = distance;
            } else {
                point = (topPolyline.getPointAtPercent(position) + bottomPolyline.getPointAtPercent(position)) / 2;
                float distance = (point - target).length();
                if (distance < result) result = distance;
                
            }
        }
        return result;
    }
    
    // Generating carriers
    TextCarrier* assignNewCarrier(float normalizedLength, float position) {
        
        TextCarrier *result = new TextCarrier();
        float start = position;
        
        float length = normalizedLength;
        if (useCentral)
            result->setup(this, start, length, 10);
        else result->setup(this, start, length, 10);
        
        result->animateTo(start, length, 1, ofGetElapsedTimef());
        
        result->color = color;
        
        carriers.push_back(result);
        recalcCarrierStats();
        
        compressCarriersToLevel();
        
        return result;
    }
    
    bool checkIfPlaceIsFree(float pos, float len) {
        bool free = true;
        for (int i = 0; i < carriers.size(); i++) {
            if (carriers[i]->isAnimating()) {
                if (Utils::isOverriding(carriers[i]->start.target, carriers[i]->length.target, pos, len))
                    free = false;
            } else {
                if (Utils::isOverriding(carriers[i]->start, carriers[i]->length, pos, len))
                    free = false;
            }
        }
        return free;
    }
    
    float getFreePlaceClosestToPoint(float length, ofPoint point) {
        float minimalPlace = -1; float minDistance = 99999;
        for (int i = 0; i < 10; i++) {
            float randomPlace = ofRandomuf();
            if (checkIfPlaceIsFree(randomPlace, length)) {
                if (useCentral) {
                    float dist = (centralPolyline.getPointAtPercent(randomPlace) - point).length();
                    if ((dist < minDistance)) {
                        minDistance = dist;
                        minimalPlace = randomPlace;
                    }
                } else {
                    float dist = ((topPolyline.getPointAtPercent(randomPlace) + bottomPolyline.getPointAtPercent(randomPlace)) / 2 - point).length();
                    if ((dist < minDistance)) {
                        minDistance = dist;
                        minimalPlace = randomPlace;
                    }
                }
            }
        }
        return minimalPlace;
    }
    
    float getFreeSpace() {
        float result = 0;
        int letters = 0;
        for (int i = 0; i < carriers.size(); i++) {
            result += carriers[i]->length;
            letters += carriers[i]->lettersRegistered;
        }
        return 1 - result;
    }
    
    float getFreeSpacePx() {
        return getFreeSpace() * getLengthInPixels();
    }
    
    int getCarrierToTheLeftOf(int idx) {
        int result = -1; float minDistance = 100000;
        float actualStart = (carriers[idx]->isAnimating() ? carriers[idx]->start.target : carriers[idx]->start);
        for (int i = 0; i < carriers.size(); i++) {
            float pierActualStart = (carriers[i]->isAnimating() ? carriers[i]->start.target + carriers[i]->length.target : carriers[i]->start + carriers[i]->length);
            float distance = actualStart - (pierActualStart);
            if ((distance < minDistance) && (distance > 0)) {
                minDistance = distance;
                result = i;
            }
        }
        return result;
    }
    

    void compressCarriersToLevel() {
        std::sort (carriers.begin(), carriers.end(), TextCarrier::sortStarts);

        float gap = getFreeSpace() / carriers.size();
        float place = gap/2;
        for (int i = 0; i < carriers.size(); i++) {
            carriers[i]->animateTo(place, carriers[i]->length.target, 0.5, ofGetElapsedTimef());
            
            place += carriers[i]->length.target;
            place += gap;
        }
    }

    
    void compressCarriers() {
        std::sort (carriers.begin(), carriers.end(), TextCarrier::sortEnds);
        
        for (int i = 0; i < carriers.size(); i++) {
            int left = getCarrierToTheLeftOf(i);
            if ((left >= 0)&&(!carriers[i]->isAnimating())) {
                carriers[i]->animateTo(carriers[left]->getLogicalStart() + carriers[left]->getLogicalLength() + 0.01, carriers[i]->length, 0.5, TimestampConsumer::timestamp + i * 0.1);
            } else if (left < 0) {
                // this is the leftest
                carriers[i]->animateTo(0, carriers[i]->length, 0.5, TimestampConsumer::timestamp + i * 0.1);
            }
        }
    }
    
    void drawOutline() {
        if (useCentral) {
            ofSetColor(255);
            ofCircle(centralPolyline.getPointAtLength(0), 3);
            centralPolyline.draw();
            ofCircle(centralPolyline.getPointAtLength(0.99), 3);
            
            ofCircle(getPointAtLeft(0), 5 + sin(ofGetElapsedTimef() * 5));

        } else {
            ofSetColor(255);
            ofCircle(topPolyline.getPointAtLength(0), 3);
            topPolyline.draw();
            bottomPolyline.draw();
            ofCircle(bottomPolyline.getPointAtLength(0), 3);
        }
        
        if (carriers.size() > 0)
            for (int i = 0; i < carriers.size(); i++) {
                ofSetColor(ofColor::fromHsb(i * 20, 200, 200));
                
                bool ovrd = false;
                bool animatingTrouble = false;
                for (int j = 0; j < carriers.size(); j++) {
                    if (i == j) continue;
                    
                    // check if this carrier overrides another one while they're both static:
                    if (Utils::isOverriding(carriers[j]->start, carriers[j]->length, carriers[i]->start, carriers[i]->length)) {
                        if ((!carriers[j]->isAnimating()) && (!carriers[i]->isAnimating())) {
                            ovrd = true;
                        } else {
                            ovrd = true;
                            animatingTrouble = true;
                        }
                    }
                }
                carriers[i]->drawOutline(1 + 3 * ovrd + sin(ofGetElapsedTimef()) * 3 * animatingTrouble);
                

            }
        
    }
    
    void deleteCarrier(int idx) {
        delete carriers[idx];
        carriers.erase(carriers.begin() + idx);
    }
    
    void recalcCarrierStats() {
        carrierSum = 0;
        for (int i = 0; i < carriers.size(); i++) {
            carrierSum += carriers[i]->length;
        }
    }
    
    float carrierSum;
    
    void update() {
        
        if (internalOffset > 1) internalOffset -= 1;
        
        // checking if we haven't got enough space:
        overflowFlag = (getFreeSpace() < 0); 
        
        for (int i = 0; i < carriers.size(); i++) {
            if (carriers[i]->lettersRegistered == 0) {
                deleteCarrier(i);
            }
        }
        
        for (int i = 0; i < carriers.size(); i++) {
            carriers[i]->update();
            
            if (carriers.size() > 1)
            for (int j = 0; j < carriers.size(); j++) {
                if (i == j) continue;
                
                // check if this carrier overrides another one while they're both static:
                if (Utils::isOverriding(carriers[j]->start, carriers[j]->length, carriers[i]->start, carriers[i]->length)) {
                    if ((!carriers[j]->isAnimating()) && (!carriers[i]->isAnimating())) {
                        compressCarriers();
                        if (carriers[j]->start > carriers[i]->start)
                            carriers[j]->start += 0.01;
                        else carriers[j]->start -= 0.01;
                    }
                }
            }
        }
    }
    
    bool overflowFlag;

    Animatable<ofColor> color;
    
    bool useCentral;
    ofPolyline centralPolyline, topPolyline, bottomPolyline;
    vector<TextCarrier*> carriers;
};

#endif
