//
//  Utils.h
//  TextMagic
//
//  Created by zebra on 21/09/15.
//
//

#ifndef TextMagic_Utils_h
#define TextMagic_Utils_h

#include "ofMain.h"

namespace Utils {

static ofVec3f getVectorRotated(ofPoint p1, ofPoint p2, float angle, float size) {
    ofVec3f vec = p2 - p1;
    ofVec3f vec2 = vec.normalize();
    vec2.rotate(angle, ofVec3f(0,0,1));
    return (vec2 * size + p1);
}

#define DEFAULT_EASING_FACTOR 2
    
static float easing(float arg) {
        return (
                pow(arg, DEFAULT_EASING_FACTOR) / (pow(arg, DEFAULT_EASING_FACTOR) + pow(1 - arg, DEFAULT_EASING_FACTOR))
                );
        
    }

static bool isOverriding (float start1, float len1, float start2, float len2) {
        bool result = false;
        if ((start1 < start2) && ((start1 + len1) > start2)) return true;
        if ((start2 < start1) && ((start2 + len2) > start1)) return true;
    
        if ((start1 <= (start2 + len2)) && ((start1 + len1) >= (start2 + len2))) return true;
        if ((start2 <= (start1 + len1)) && ((start2 + len2) >= (start1 + len1))) return true;
        return false;
    }
    
static ofPoint intersection(ofPoint p1, ofPoint p2, ofPoint p3, ofPoint p4) {
    // Store the values for fast access and easy
    // equations-to-code conversion
    float x1 = p1.x, x2 = p2.x, x3 = p3.x, x4 = p4.x;
    float y1 = p1.y, y2 = p2.y, y3 = p3.y, y4 = p4.y;
    
    float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    // If d is zero, there is no intersection
    if (d == 0) return ofPoint(0) /* NULL */;
    
    // Get the x and y
    float pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
    float x = ( pre * (x3 - x4) - (x1 - x2) * post ) / d;
    float y = ( pre * (y3 - y4) - (y1 - y2) * post ) / d;
    
    // Check if the x and y coordinates are within both lines
    if ( x < min(x1, x2) || x > max(x1, x2) ||
        x < min(x3, x4) || x > max(x3, x4) ) return ofPoint(0) /* NULL */;
    if ( y < min(y1, y2) || y > max(y1, y2) ||
        y < min(y3, y4) || y > max(y3, y4) ) return ofPoint(0) /* NULL */;
    
    // Return the point of intersection
    ofPoint ret = ofPoint();
    ret.x = x;
    ret.y = y;
    return ret;
}
    
static float sign (ofPoint p1, ofPoint p2, ofPoint p3)
{
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}
    
    
static bool PointInTriangle (ofPoint pt, ofPoint v1, ofPoint v2, ofPoint v3)
{
    bool b1, b2, b3;
    
    b1 = sign(pt, v1, v2) < 0.0f;
    b2 = sign(pt, v2, v3) < 0.0f;
    b3 = sign(pt, v3, v1) < 0.0f;
    
    return ((b1 == b2) && (b2 == b3));
}
    

static bool pointOutsideQuad(ofPoint arg, ofPoint *quad) {
    return ((!PointInTriangle(arg, quad[0], quad[1], quad[2])) && (!PointInTriangle(arg, quad[0], quad[2], quad[3])));
}



    

// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
static bool onSegment(ofPoint p, ofPoint q, ofPoint r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
        q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
        return true;
    
    return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
static int orientation(ofPoint p, ofPoint q, ofPoint r)
{
    // See 10th slides from following link for derivation of the formula
    // http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
    int val = (q.y - p.y) * (r.x - q.x) -
    (q.x - p.x) * (r.y - q.y);
    
    if (val == 0) return 0;  // colinear
    
    return (val > 0)? 1: 2; // clock or counterclock wise
}

// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
static bool doIntersect(ofPoint p1, ofPoint q1, ofPoint p2, ofPoint q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
    
    // General case
    if (o1 != o2 && o3 != o4)
        return true;
    
    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;
    
    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;
    
    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;
    
    // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;
    
    return false; // Doesn't fall in any of the above cases
}

}

#endif
