//
//  AnimatableProperty.h
//  TextMagicAPI
//
//  Created by zebra on 06/10/15.
//
//

#ifndef AnimatableProperty_h
#define AnimatableProperty_h

#include "ofMain.h"

class TimestampConsumer {
public:
    static double timestamp;
    static double timepassed;
    static void updateTimestamp() {
        double newVal = ofGetElapsedTimef();
        timepassed = newVal - timestamp;
        timestamp = newVal;
    };
};

template <typename T>
class Animatable: TimestampConsumer {
public:
    // Base
    Animatable() {
        inner = T();
        timeStarted = ofGetElapsedTimef();
        animationTime = 0;
    }
    
    
    
    //////////
    
    operator T & () {
        animate();
        return inner;
    }
    
    void operator=(const T & v) {
        inner = v;
        target = v;
    }
    
    T operator+(const T &v) {
        return inner + v;
    }
    
//    bool operator<(const T &v) {
//        return inner < v;
//    }
    
    T& get() {
        animate();
        return inner;
    }
    
    // Animation
    float easing(float arg) {
        return (
                pow(arg, 3) / (pow(arg, 3) + pow(1 - arg, 3))
                );
    }
    
    bool isAnimating() {
        if ((timestamp - timeStarted) > animationTime) inner = target;
        if (timestamp < timeStarted) return false;
        return ((timestamp - timeStarted) < animationTime);
    };
    
    void animate() {
        if (isAnimating()) {
            float phase = easing(((timestamp - timeStarted) / animationTime));
            inner = target * phase + previous * (1 - phase);
        } 
    }
    
    void animateTo(T target_, float time = 1) {
        previous = inner;
        target = target_;
        animationTime = time;
        timeStarted = ofGetElapsedTimef();
    }

    void animateTo(T target_, double timeAnimationStarted, float time = 1) {
        previous = inner;
        target = target_;
        animationTime = time;
        timeStarted = timeAnimationStarted;
    }
    
//    static void hiFromClass() {
//        //        count++;
//    }
    
    //protected:
    T inner;
    T target;
    T previous;
    double animationTime, timeStarted;
};

#endif /* AnimatableProperty_h */
