//
//  TextCarrierSystem.h
//  TextMagicAPI
//
//  Created by zebra on 01/10/15.
//
//

#ifndef TextMagicAPI_TextCarrierSystem_h
#define TextMagicAPI_TextCarrierSystem_h

#include "TextCarrierHost.h"
#include "TextFlexer.h"

class TextCarrierSystem {
public:
    TextCarrierSystem() {
        isFull = false;
        sizeModifier = ofPoint(1);
        offsetModifier = ofPoint(0);
    }
    
    virtual void updateSystem() {
        
    }
    
    void drawOutline() {
        for (int i = 0; i < lines.size(); i++) {
            lines[i].drawOutline();
        }
    }
    
    virtual void update() {
        for (int i = 0; i < lines.size(); i++) {
            lines[i].update();
        }
    }
    
    void clear() {
        while (lines.size() > 0) {
            lines.pop_back();
        }
    }
    
    bool isFull;
    
    TextCarrier* assignCarrier(TextFlexer *text) {
        int index = -1; int tries = 0;
        while ((index < 0) && (tries < lines.size() * 2)) {
            int tr = ofRandom(lines.size());
            if (lines[tr].getFreeSpacePx() > text->getLength() * 1/*.2*/)
                index = tr;
            // DEBUG:
            else {
                ofLog() << "free space: " << lines[tr].getFreeSpacePx() << " "
                    << "required space: " << text->getLength() * 1.2;
            }
            tries++;
        }
        if (index >= 0) { // we found suitable host
            ofLog() << "free space where we're moving:" << lines[index].getFreeSpacePx() << " ; space required: " << text->getLength() << " phrase:" << text->getText();
            float textLen = text->getLength() / lines[index].getLengthInPixels();
            ofLog() << "calling CCCCC with len = " << text->getLength();
            return lines[index].assignNewCarrier(textLen, lines[index].getFreePlaceClosestToPoint(textLen, ofPoint(text->getCentroid())));;
        } else {
            ofLog() << "NO SUITABLE HOST? (basic assigncarrier)";
            bool fullTest = true;

            return NULL;
        }
    }

    //
    TextCarrier* assignCarrierClosestToPoint(TextFlexer *text, ofPoint target, int howManyTries) {

        int index = -1; int tries = 0;
        int minimalHost = -1; float minimalDistance = 999999;
        for (int i = 0; i < howManyTries; i++) {
            int host = ofRandom(lines.size());
            if (lines[host].getFreeSpacePx() < (text->getLength() * 1.2)) continue;
            float distance = lines[host].getMinimalPossibleDistanceToPoint(target, howManyTries);
            if (distance < minimalDistance) {
                minimalHost = host;
                minimalDistance = distance;
            }
        }
        
        index = minimalHost;
        
        if (index >= 0) { // we found suitable host
            ofLog() << "free space wheore we're moving:" << lines[index].getFreeSpacePx() << " ; space required: " << text->getLength() << " phrase:" << text->getText();
            float textLen = text->getLength() / lines[index].getLengthInPixels();
            ofLog() << "calling CCCCC with len = " << text->getLength();
            ofLog() << "looking for carrier for textlen:" << textLen;
            TextCarrier *result = lines[index].assignNewCarrier(textLen, lines[index].getFreePlaceClosestToPoint(textLen, target));
            if (result->getLengthInPixels() > 100) {
                ofLog() << "::::::::::::: result length:" << result->getLengthInPixels();
            }
            return result;
        } else {
            bool fullTest = true;

            
            // Disappear
            ofPolyline line;
            line.addVertex(text->getCentroid());
            line.addVertex(text->getCentroid() + ofPoint(150, 0));
            return text->assignInnerHostCarrier(line);
        }
    }
    
//////////
    
    TextCarrierHost* addLine(ofPolyline line) {
        TextCarrierHost ch;
        ch.centralPolyline = line;
        ch.useCentral = true;
        lines.push_back(ch);
        return &lines[lines.size() - 1];
    }

    TextCarrierHost* addSegment(ofPolyline top, ofPolyline bottom) {
        TextCarrierHost ch;
//        ch.centralPolyline = line;
        ch.topPolyline = top;
        ch.bottomPolyline = bottom;
        ch.useCentral = false;
        lines.push_back(ch);
        return &lines[lines.size() - 1];
    }
    
//private:
    vector<TextCarrierHost> lines; //TODO: change to TextCarrierDataProvider
    
    ofPoint offsetModifier, sizeModifier;
    float textWidthMultiplier;
    
    std::string description;
};

#endif
