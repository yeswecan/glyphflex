//
//  OpenOpenOfxTrueTypeFontUC.h
//  ofxGlyphFlexExample
//
//  Created by zebra on 15/06/15.
//
//

#ifndef __ofxGlyphFlexExample__OpenOpenOfxTrueTypeFontUC__
#define __ofxGlyphFlexExample__OpenOpenOfxTrueTypeFontUC__

#include <vector>
#include "ofRectangle.h"
#include "ofPath.h"

//--------------------------------------------------
const static string OF_OTTFUC_SANS = "sans-serif";
const static string OF_OTTFUC_SERIF = "serif";
const static string OF_OTTFUC_MONO = "monospace";


//--------------------------------------------------
class OpenOfxTrueTypeFontUC{
public:
    OpenOfxTrueTypeFontUC();
    virtual ~OpenOfxTrueTypeFontUC();
    
    // -- default (without dpi), anti aliased, 96 dpi:
    bool loadFont(const string &filename, int fontsize, bool bAntiAliased=true, bool makeContours=false, float simplifyAmt=0.3, int dpi=0);
    void reloadFont();
    
    
    void drawString(const string &utf8_string, float x, float y, int symbolIndex);
    void drawString(const string &utf8_string, float x, float y);
    void drawStringAsShapes(const string &utf8_string, float x, float y);
    
    float getTTFKerning(unsigned int c);
    
    vector<ofPath> getStringAsPoints(const string &utf8_string, bool vflip=ofIsVFlipped());
    ofRectangle getStringBoundingBox(const string &utf8_string, float x, float y);
    
    bool isLoaded();
    bool isAntiAliased();
    
    int getSize();
    float getLineHeight();
    void setLineHeight(float height);
    float getLetterSpacing();
    void setLetterSpacing(float spacing);
    float getSpaceSize();
    void setSpaceSize(float size);
    float stringWidth(const string &utf8_string);
    float stringHeight(const string &utf8_string);
    // get the num of loaded chars
    int getNumCharacters();
    ofTextEncoding getEncoding() const;
    // set the default dpi for all typefaces
    static void setGlobalDpi(int newDpi);
    
    
    
private:
    class Impl;
    Impl *mImpl;
    
    // disallow copy and assign
    OpenOfxTrueTypeFontUC(const OpenOfxTrueTypeFontUC &);
    void operator=(const OpenOfxTrueTypeFontUC &);
};

//--------------------------------------------------
namespace util {
    namespace OpenOfxTrueTypeFontUC {
        basic_string<unsigned int> convUTF8ToUTF32(const string & utf8_string);
    }
}

#endif /* defined(__ofxGlyphFlexExample__OpenOpenOfxTrueTypeFontUC__) */
