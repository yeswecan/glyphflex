//
//  FlexLetter.h
//  ofxGlyphFlexExample
//
//  Created by zebra on 15/06/15.
//
//

#ifndef ofxGlyphFlexExample_FlexLetter_h
#define ofxGlyphFlexExample_FlexLetter_h

#include "ofxHomography.h"
#include "MyGLWarper.h"

static void mapMatrixToRect(ofPoint p1, ofPoint p2, ofPoint p3, ofPoint p4, ofPoint sizeIn, MyGLWarper *warper) {
    
//    ofPoint originalCorners[4];
    warper->originalCorners[0] = ofPoint(0);
    warper->originalCorners[1] = ofPoint(sizeIn.x, 0);
    warper->originalCorners[2] = sizeIn;
    warper->originalCorners[3] = ofPoint(0, sizeIn.y);
    
//    ofPoint distortedCorners[4];
    warper->distortedCorners[0] = p1;
    warper->distortedCorners[1] = p2;
    warper->distortedCorners[2] = p3;
    warper->distortedCorners[3] = p4;
    
    warper->homography = ofxHomography::findHomography(warper->originalCorners, warper->distortedCorners);
    ofPushMatrix();
    glMultMatrixf(warper->homography.getPtr());
    

}

static void unmapMatrix(MyGLWarper *warper) {
//    warper->end();
    ofPopMatrix();
}

#include "OpenOfxTrueTypeFontUC.h"
#include "Utils.h"
#include "TextCarrier.h"
#include "Animatable.h"

class FlexLetter {
public:
    FlexLetter(string letter_) {
        ofLog() << "LETTER = " << (letter_);
        letter = letter_;
        color = ofColor(0);
        customColor = false;
        visible = true;
        
        cached = false;
        
        targetCarrier = NULL;
        previousCarrier = NULL;
        
        useCache = false;
        
    }
    
    void draw() {
        if (!visible) return;
        if ((ofGetKeyPressed('h')) && (carrierTransition == 1)) {
            return;
        }
        if (Utils::doIntersect(currentPosition[0],
                               currentPosition[1],
                               currentPosition[2],
                               currentPosition[3]))
            return;
        if (Utils::doIntersect(currentPosition[1],
                               currentPosition[2],
                               currentPosition[3],
                               currentPosition[0]))
            return;
        
        if (Utils::pointOutsideQuad(Utils::intersection(currentPosition[0], currentPosition[2], currentPosition[1], currentPosition[3]), currentPosition))
            return;

        
        ofPoint innerSize = ofPoint(kerning * 3.84, 50);
        
        mapMatrixToRect(currentPosition[0], currentPosition[1], currentPosition[2], currentPosition[3], innerSize, warper);
        ofTranslate(100 * perLetterOffset.x, 100 * perLetterOffset.y);
        ofScale((1.62 / 3) * (1.75 + 1.0 * (perLetterScaleMultiplier.x)),
                (1.76 / 3) * (1.75 + 1.0 * (perLetterScaleMultiplier.y)));
        if (customColor)
            ofSetColor(color);
        if (!useCache) {
            ofScale(3, 3);
            font->drawStringAsShapes(letter, 0, font->getSize() * 0.94);
        } else {
            cache.draw(0,0);
        }
        unmapMatrix(warper);
    }
    
    void drawOutline() {
        ofLine(currentPosition[0], currentPosition[1]);
        ofLine(currentPosition[1], currentPosition[2]);
        ofLine(currentPosition[2], currentPosition[3]);
        ofLine(currentPosition[3], currentPosition[0]);
        
        ofSetColor(255);
        ofDrawBitmapString(ofToString(kerning), currentPosition[2]);
        
    }

    
    // the following animateTo is a way to future
    void animateTo(TextCarrier *nextCarrier, float animationStart, float timeframe, float start, float length, bool startFromCurrentPoints = false) {
        if (previousCarrier != NULL) { // this happens cause we'll be replacing
                                       // previous carrier with the new previous carrier
            previousCarrier->unregisterLetter();
        }
        color.animateTo(nextCarrier->getColor(), animationStart, 1);
        previousCarrier = targetCarrier;
        targetCarrier = nextCarrier;
        targetCarrier->registerLetter();
        
        carrierStart.animateTo(start, animationStart, timeframe);
        carrierLength.animateTo(length, animationStart, timeframe);
        
        if (startFromCurrentPoints) {
            useCustomPreviousPoints = true;
            customPreviousPoints[0] = currentPosition[0];
            customPreviousPoints[1] = currentPosition[1];
            customPreviousPoints[2] = currentPosition[2];
            customPreviousPoints[3] = currentPosition[3];
        } else useCustomPreviousPoints = false;
            carrierTransition = 0;
            carrierTransition.animateTo(1, animationStart, timeframe);
    }
    
    
    float easing(float arg) {
        return (
                pow(arg, DEFAULT_EASING_FACTOR) / (pow(arg, DEFAULT_EASING_FACTOR) + pow(1 - arg, DEFAULT_EASING_FACTOR))
                );
        
    }
    
    void setColor(ofColor color_) {
        customColor = true;
        color = color_;
    }
    
    ofPoint getCentroid() {
        return (currentPosition[0] + currentPosition[1] + currentPosition[2] + currentPosition[3]) / 4;
    }
    
    void update() {
        ofPoint targetCarrierPoints[4];
        targetCarrier->get4Points(targetCarrierPoints, (float)carrierStart, (float)carrierLength);

//        The following will make the letters bend like http://www.filmi-hd.ru/_ld/2/04439761.png
//        currentPosition[0] = carrierTransition * targetCarrier->getPointAtLeft(carrierStart);
//        currentPosition[1] = carrierTransition * targetCarrier->getPointAtLeft(carrierStart + carrierLength);
//        currentPosition[2] = carrierTransition * targetCarrier->getPointAtRight(carrierStart + carrierLength);
//        currentPosition[3] = carrierTransition * targetCarrier->getPointAtRight(carrierStart);
        currentPosition[0] = carrierTransition * targetCarrierPoints[0];
        currentPosition[1] = carrierTransition * targetCarrierPoints[1];
        currentPosition[2] = carrierTransition * targetCarrierPoints[2];
        currentPosition[3] = carrierTransition * targetCarrierPoints[3];
        if (useCustomPreviousPoints) {
            currentPosition[0] += (1 - carrierTransition) * customPreviousPoints[0];
            currentPosition[1] += (1 - carrierTransition) * customPreviousPoints[1];
            currentPosition[2] += (1 - carrierTransition) * customPreviousPoints[2];
            currentPosition[3] += (1 - carrierTransition) * customPreviousPoints[3];
        } else
        if (previousCarrier != NULL) {
            ofPoint previousCarrierPoints[4];
            previousCarrier->get4Points(previousCarrierPoints, carrierStart, carrierLength);
            currentPosition[0] += (1 - carrierTransition) * previousCarrierPoints[0];
            currentPosition[1] += (1 - carrierTransition) * previousCarrierPoints[1];
            currentPosition[2] += (1 - carrierTransition) * previousCarrierPoints[2];
            currentPosition[3] += (1 - carrierTransition) * previousCarrierPoints[3];
        }

        isAnimating = (carrierTransition < 1);
        
        if ((carrierTransition > 0.9999) && (previousCarrier != NULL)) {
            previousCarrier->unregisterLetter();
            previousCarrier = NULL;
            carrierTransition = 1;
        }
    }
    
    // Carrier-related
    TextCarrier *targetCarrier , *previousCarrier;
    Animatable<float> carrierStart, carrierLength, carrierTransition;
    
    // Non carrier-related
    ofPoint customPreviousPoints[4];
    
    // Resources
    OpenOfxTrueTypeFontUC *font;
    
    // Per letter fixes
    static ofPoint perLetterOffset;
    static ofPoint perLetterScaleMultiplier;
    
    // Base stuff
    bool cached;
    bool useCache;
    bool useCustomPreviousPoints;
    ofFbo cache;
    float animationStartedTime = -1;
    float animationTimeframe = 0;
    MyGLWarper *warper;
    ofPoint previousPosition[4];
    ofPoint targetPosition[4];
    ofPoint currentPosition[4];
    bool isAnimating = false;
    string letter;
    
    float kerning;
    
    Animatable<ofColor> color;
    bool customColor;
    
    //
    bool visible;
};

#endif
