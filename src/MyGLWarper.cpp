#include "MyGLWarper.h"
#include "stdio.h"


//--------------------------------------------------------------
void MyGLWarper::activate(){
//	ofRegisterMouseEvents(this);
}
//--------------------------------------------------------------
void MyGLWarper::deactivate(){
//	ofUnregisterMouseEvents(this);
}

//--------------------------------------------------------------
void MyGLWarper::mouseReleased(ofMouseEventArgs &args){
    if(whichCornerMy>=0)
    {
        processCorners();
    }
	whichCornerMy = -1;
}

//--------------------------------------------------------------
void MyGLWarper::mouseMoved(ofMouseEventArgs &args){}

//--------------------------------------------------------------
void MyGLWarper::mouseDragged(ofMouseEventArgs &args){

    if(whichCornerMy>=0)
    {
        pointsFrom[whichCornerMy].x = point.x;
        pointsFrom[whichCornerMy].y = point.y;
        
        pointsTo[whichCornerMy].x = args.x;
        pointsTo[whichCornerMy].y = args.y;
        
        processMatricesFromTo();
    }
}

//--------------------------------------------------------------
void MyGLWarper::mousePressed(ofMouseEventArgs &args){
    
	whichCornerMy = -1;
    
    float smallestDist = std::numeric_limits<float>::max();

    point = fromScreenToWarpCoord(args.x,args.y,0);//ofPoint(args.x,args.y);
    
    for(int i = 0; i < 4; i++){
        //zofPoint p = getCorner((CornerLocation)i);
        
        ofVec4f vec = fromScreenToWarpCoord(pointsTo[i].x,pointsTo[i].y,0);
        
        //ofLog() << point << ", " << corner << endl;
        
        float distx = point.x - vec.x;
        float disty = point.y - vec.y;
        
        float dist  = sqrt( distx * distx + disty * disty);
        
        if(dist < smallestDist){
            whichCornerMy = i;
            smallestDist = dist;
        }
    }
 
    ofLog() << "whichCornerMy: "<< whichCornerMy << endl;
}


