//
//  TextCarrierDataProvider.h
//  TextMagicAPI
//
//  Created by zebra on 05/10/15.
//
//

#ifndef TextCarrierDataProvider_h
#define TextCarrierDataProvider_h

// That is all the things carrier asks for from the host
class TextCarrierDataProvider {
public:
	virtual float getLengthInPixels() { return 0; };
    
	virtual ofPoint getPointAtLeft(float position) { return ofPoint(0); };
    
	virtual ofPoint getPointAtRight(float position) { return ofPoint(0); };

    virtual void get4Points(ofPoint *points, float start, float length) {};
    
    // TODO: implement this
	virtual float getHeightAtPercent(float position) { return 0; };
};

#endif /* TextCarrierDataProvider_h */
