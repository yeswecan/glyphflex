//
//  TextCarriers.h
//  ofxGlyphFlexExample
//
//  Created by zebra on 04/09/15.
//
//

#ifndef ofxGlyphFlexExample_TextCarriers_h
#define ofxGlyphFlexExample_TextCarriers_h
#include "ofMain.h"
#include "Utils.h"
#include "TextCarrierDataProvider.h"
#include "Animatable.h"

class TextCarrier {
public:
    TextCarrier() {
        height = 20;
        
        animationEndTime = -1; animationStartTime = 0;
        
        length = 1;
        start = 0;
        lettersRegistered = 0;
        
    }

    void setup(TextCarrierDataProvider *tcDataProvider, float newStart, float newLength, float newHeight) {
        dataProvider = tcDataProvider;
        height = newHeight;
        start = newStart;
        length = newLength;
    }
    
    void animateTo(float newStart, float newLength, float animationDuration, double animationStartTime_) {
        start.animateTo(newStart, animationStartTime_, animationDuration);
        length.animateTo(newLength, animationStartTime_, animationDuration);
    }
    
    bool isAnimating() {
        return (animationEndTime > TimestampConsumer::timestamp);
    }

    // Base functions
    
    float getLogicalLength() {
        if (length.isAnimating()) return length.target;
            else return length;
    }
    
    float getLogicalStart() {
        if (start.isAnimating()) return start.target;
            else return length;
    }
    
    ofColor getColor() {
        return color;
    }
    
    float getLengthInPixels() {
        return dataProvider->getLengthInPixels() * length;
    }
    
    ofPoint getPointAtLeft(float position) {
        return dataProvider->getPointAtLeft(start + position * length);
    }

    ofPoint getPointAtRight(float position) {
        return dataProvider->getPointAtRight(start + position * length);
    }
    
    virtual void get4Points(ofPoint *points, float startInCarrier, float lengthInCarrier) {
        dataProvider->get4Points(points, start + startInCarrier * length, length * lengthInCarrier);
    }
    
    // Letters reference counting
    
    int lettersRegistered;
    
    void registerLetter() {
        lettersRegistered ++;
    }
    
    void unregisterLetter() {
        lettersRegistered --;
    }
    
    // Debug drawing
    
    void drawOutline(float lineWidth = 1) {
        if (getLengthInPixels() > 10) {

            float tempPhase = (sin(ofGetElapsedTimef()) + 1) / 2;
            ofLine(getPointAtLeft(tempPhase), getPointAtRight(tempPhase));
            ofPoint zz = getPointAtLeft(tempPhase);
            
                ofPoint points[4];
                get4Points(points, 0, 1);
                ofPoint p1 = points[0];
                ofPoint p2 = points[1];
                ofPoint p3 = points[2];
                ofPoint p4 = points[3];
            
                ofSetLineWidth(lineWidth);
                ofLine(p1, p2);
                ofLine(p2, p3);
                ofLine(p3, p4);
                ofLine(p4, p1);
                ofSetLineWidth(1);
            
            
            if (isAnimating()) {
                ofDrawBitmapString("A", p1);
            }
//            }
 

            int c = getLengthInPixels() / 10;

            ofSetCircleResolution(4);
            for (int i = 0; i < c; i++) {
                float phase = (float)i / (float)c;
                float realPhase = phase * length + start;
            }
            ofSetCircleResolution(8);
            
 
            ofSetColor(255);
 
        }
    }

    
    void update() {
        // TODO:
        // strange things happen when you remove this.
        // Investigate it
        float realPhase = length + start;
    }
    
    bool operator < ( TextCarrier& tc)
    {
        return ((start + length) < (tc.start + tc.length));
    }
    
    static bool sortEnds(TextCarrier *i, TextCarrier *j) {
        return ((i->start) < (j->start));
    }

    static bool sortStarts(TextCarrier *i, TextCarrier *j) {
        return ((i->start + i->length) < (j->start + j->length));
    }
    
    // base
    TextCarrierDataProvider *dataProvider;
    Animatable<float> start, length, height;
    ofColor color;

    // animation
    double animationStartTime, animationEndTime;
    
    
private:
};
#endif
