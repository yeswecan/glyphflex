//
//  textFlexer.h
//  openFrameworksLib
//
//  Created by zebra on 05/03/15.
//
//

#ifndef openFrameworksLib_textFlexer2_h
#define openFrameworksLib_textFlexer2_h

#include "OpenOfxTrueTypeFontUC.h"
#include "MyGLWarper.h"
#include "FlexLetter.h"
#include "TextCarrierHost.h"

class TextFlexer {
public:
    TextFlexer (string fontName) {
        font = new OpenOfxTrueTypeFontUC();
        font->loadFont(fontName, 45, true, true, true);
        warper_ = new MyGLWarper();
        lettersTimeOffset = 0;
        alternativeAlgo = false;
        useTargetStartPosition = true;

        setWidthMultiplier(1);
    }
    
    TextFlexer() {
        warper_ = new MyGLWarper();
        lettersTimeOffset = 0;
        alternativeAlgo = false;
        
        setWidthMultiplier(1);
    };
    
    vector<FlexLetter*> letters;
    
    // =========================================================================================
    // ============================= –£—Ç–∏–ª–∏—Ç—ã –∏ –Ω–∞—Å—Ç—Ä–æ–π–∫–∞ =======================================
    
    // –°–æ–∑–¥–∞–Ω–∏–µ –±—É–∫–≤
    void createLetters(string arg);

    // –û—Å—É—â–µ—Å—Ç–≤–ª—è–µ—Ç—Å—è –ª–∏ –≤ –¥–∞–Ω–Ω—ã–π –º–æ–º–µ–Ω—Ç –∞–Ω–∏–º–∞—Ü–∏—è –≤ —ç—Ç–æ–º –±–ª–æ–∫–µ —Ç–µ–∫—Å—Ç–∞?
    bool areLettersAnimating();

    void animateToCarrier(TextCarrier *tc, float animationTime = 0, float animationStartTime = 0, float perLetterOffset = 0, bool startFromCurrentPoints = false);

    
    void setLettersPositionsAnimated(float desiredWidth, float animationStartedTime, float animationTimeframe, float start, float end, float desiredHeight);
    
    void setLettersPositionsAnimated(float desiredWidth, float animationStartedTime, float animationTimeframe, vector<float> desiredHeights, float start, float end, bool useHeightMap, float desiredHeight);
    
    void setLettersPathsAnimated(ofPolyline top, ofPolyline bottom, float animationStartedTime, float animationTimeframe, float pathStart, float pathEnd, float textWidth = 30, float lettersTimeOffset_ = 0);
    
    void setWidthMultiplier(float arg);
    
    TextCarrier* assignInnerHostCarrier(ofPolyline c, float h = 0) {
        innerHost.centralPolyline = c;
        innerHost.useCentral = true;
        innerHost.height = h;
        
        return innerHost.assignNewCarrier(1, 0);
    }
    
    ofPoint getCentroid() {
        ofPoint result;
        for (int i = 0; i < letters.size(); i++) {
            result += letters[i]->getCentroid();
        }
        result /= letters.size();
        return result;
    }
    float getLength();
    string getText();
    
    void drawLetters(bool useInnerColor = false);
    void update();
    void drawOutline();
    
    OpenOfxTrueTypeFontUC *font;
    ofPolyline line_top, line_bottom;
    
    bool useTargetStartPosition;
    
    TextCarrierHost innerHost;
    
    TextCarrier *targetCarrier;
    

protected:
    ofPoint getTopPointAtPercent(float percent);
    ofPoint getBottomPointAtPercent(float percent);
    float getTopPerimeter();
    float getBottomPerimeter();
    float sampleHeightFromVectorAt(float arg, vector<float> vec);
//    TextCarrier *carrier;
    bool alternativeAlgo;
    
    float widthMultiplier;

    struct line {
        ofPoint start, end;
        int startindex, endindex;
        float len;
    };
    
    
    int getShortestIndex(vector<line> lines_);
    void translatePaths(ofPoint factor);
    
    MyGLWarper *warper_;
    
    float lettersTimeOffset;
    
    ofPolyline currentPolyline;
    basic_string<unsigned int> text;
    string basic_string_text;
    
};

#endif
