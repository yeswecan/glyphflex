#include "SvgMovieSystem.h"
#include "TextFlexer.h"
#include "Utils.h"
#include "TextCarrierSystem.h"

//---------------------------------------------------------------------------
//
//           ofxGlyphFlex is text bending and animating engine.
//  Let me tell you how it works. On top everything you have a thing
//  called TextCarrierSystem. It holds a bunch of lines (or shapes)
//  called TextCarrierHost. The system as a whole forms an image. A single
//  TextCarrierHost is a part of it. It may consist of a single line
//  and then it should have its height set up programmaticaly; otherwise
//  it's made up of two lines, one of which is a top line and another one
//  is bottom. To debug your system, use a debug drawing.
//
//  The app itself holds both system and the words that added to it. The
//  words can move from one TextCarrierHost to another one, and the new
//  place is always given by the corresponding system. The words are called
//  TextFlexers. The place of attachment of a word to a Host is called
//  TextCarrier. TextFlexers have singular letters, called FlexLetters,
//  inside.
//
//  Hence, to add a new word to a system, you should first create it in a
//  form of a TextFlexer. Then you should make a System find a suitable
//  place for it (assignCarrierClosestToPoint) in a form of a new TextCarrier.
//  Then, you should animate your newly created TextFlexer to this TextCarrier
//  made just for it (animateToCarrier).
//
//  If you want a word to disappear, you can animate it to inner host that is
//  embedded within every TextFlexer: animateToCarrier(assignInnerHostCarrier)
//  By adding points to inner host, you can control the exact way disappearing
//  (or appearing) will occur.