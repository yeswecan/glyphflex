ofxGlyphFlex
============

Overview
--------------
This addon is made to draw smoothly animated pieces of text in different shapes. It can take series
of animated SVG files and some text strings as input.

Dependencies
--------------
ofxOpenCv, ofxTrueTypeFontUC, ofxXmlSettings, ofxSVG