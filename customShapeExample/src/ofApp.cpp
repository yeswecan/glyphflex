#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    svg.load("svg.svg");
    top = svg.getPathAt(0).getOutline()[0];
    bottom = svg.getPathAt(1).getOutline()[0];

    exampleText.line_top = top;
    exampleText.line_bottom = bottom;
    exampleText.font = new OpenOfxTrueTypeFontUC;
    exampleText.font->loadFont("days.otf", 30, false, true);
    
    exampleText.createLetters("PRIVET MEDVED");
    for (int i = 0; i < exampleText.letters.size(); i++) {
        exampleText.letters[i]->setColor(ofColor(ofRandom(255), ofRandom(255), ofRandom(255)));
        exampleText.letters[i]->color.setBrightness(255);
    }
    exampleText.setLettersPathsAnimated(top, // top path
                                        bottom, // bottom path
                                        ofGetElapsedTimef(), // animation started time
                                                             // (most of time - just now)
                                        1, // animation duration (in seconds)
                                        0, // path start position, normalized
                                        1, // path end position
                                        60, // desired symbol width in pixels (0 for auto)
                                        0.2 // time offset per symbol in seconds
                                        );
}

//--------------------------------------------------------------
void ofApp::update(){
    exampleText.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(255);
        exampleText.drawLetters();
        ofSetColor(0);
        top.draw();
        bottom.draw();
    
    exampleText.font->drawStringAsShapes("Press space to morph to new shape", 75, 75);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == 32) {
        top.clear();
        bottom.clear();
        
        top.addVertex(ofPoint(ofRandom(ofGetWidth() / 2), ofGetHeight() / 2));
        bottom.addVertex(top.getVertices()[0] + ofPoint(0, 150));
        for (int i = 1; i < 10; i++) {
            top.addVertex(top.getVertices()[i - 1] + ofPoint(ofRandom(100), ofRandom(50)) -  ofPoint(0, 25));
            bottom.addVertex(bottom.getVertices()[i - 1] + ofPoint(ofRandom(100), ofRandom(50)) -  ofPoint(0, 25));
        }
        exampleText.setLettersPathsAnimated(top, bottom, ofGetElapsedTimef(), 1, 0, 1, 60, 0.07);
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
